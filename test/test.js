const request = require('supertest');
const app = require('../server');

describe('App', () => {
  it('has token required on default page ', (done) => {
    request(app).get('/').expect({ message: 'No token provided' }, done);
  });
  it('has healthcheck page', (done) => {
    request(app)
      .get('/healthcheck')
      .expect({ message: 'healthcheck alive' }, done);
  });
});
