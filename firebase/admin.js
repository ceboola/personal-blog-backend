const firebase = require('firebase-admin');

const credentials = require('./credentials');

firebase.initializeApp({
  credential: firebase.credential.cert(credentials()),
  databaseURL: '<yourproject>.firebaseio.com',
});

module.exports = firebase;
