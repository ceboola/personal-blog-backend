require('dotenv').config();

const dataCredentials = {
  type: process.env.FIREBASE_TYPE,
  project_id: process.env.FIREBASE_PROJECTID,
  private_key_id: process.env.FIREBASE_PRIVATEKEYID,
  private_key: process.env.FIREBASE_PRIVATEKEY.replace(/\\n/g, '\n'),
  client_email: process.env.FIREBASE_CLIENTEMAIL,
  client_id: process.env.FIREBASE_CLIENTID,
  auth_uri: process.env.FIREBASE_AUTHURI,
  token_uri: process.env.FIREBASE_TOKENURI,
  auth_provider_x509_cert_url: process.env.FIREBASE_AUTHPROVIDERCERT,
  client_x509_cert_url: process.env.FIREBASE_CLIENTCERT,
  apiKey: process.env.FIREBASE_APIKEY,
  authDomain: process.env.FIREBASE_AUTHDOMAIN,
  projectId: process.env.FIREBASE_PROJECTID,
  storageBucket: process.env.FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGEID,
  appId: process.env.FIREBASE_APPID,
  measurementId: process.env.FIREBASE_MEASURMENTID,
};

const credentials = () => dataCredentials;

module.exports = credentials;
