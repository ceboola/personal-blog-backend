const firebase = require('./firebase/admin');

const getAuthToken = (req, res, next) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    const array = req.headers.authorization.split(' ');
    const [, firstIndex] = array;
    req.authToken = firstIndex;
  } else {
    req.authToken = null;
  }
  next();
};

function checkIfAdmin(req, res, next) {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = req;
      const userInfo = await firebase.auth().verifyIdToken(authToken);
      const user = await firebase.auth().getUser(userInfo.uid);
      const {
        customClaims: { admin },
      } = user;

      if (admin === true) {
        req.authId = userInfo.uid;
        return next();
      }

      throw new Error('unauthorized');
    } catch (e) {
      return res
        .status(401)
        .send({ error: 'You are not authorized to make this request' });
    }
  });
}

module.exports = checkIfAdmin;
