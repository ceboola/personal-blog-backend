const crypto = require('crypto');

const algorithm = 'aes-256-cbc';
const IV = crypto.randomBytes(16);

const { ENCRYPTION_KEY } = process.env;

const encrypt = (text) => {
  const cipher = crypto.createCipheriv(
    algorithm,
    Buffer.from(ENCRYPTION_KEY),
    IV,
  );
  let encrypted = cipher.update(text);

  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return `${IV.toString('hex')}:${encrypted.toString('hex')}`;
};

const decrypt = (text) => {
  let decrypted;
  try {
    const textParts = text.split(':');
    const iv = Buffer.from(textParts.shift(), 'hex');
    const encryptedText = Buffer.from(textParts.join(':'), 'hex');
    const decipher = crypto.createDecipheriv(
      algorithm,
      Buffer.from(ENCRYPTION_KEY),
      iv,
    );
    decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);
  } catch (error) {
    decrypted = error;
  }
  return decrypted.toString();
};

module.exports = { decrypt, encrypt };
