const firebase = require('./firebase/admin');

function authMiddleware(request, response, next) {
  const headerToken = request.headers.authorization;
  if (!headerToken) {
    return response.status(401).send({ message: 'No token provided' });
  }

  if (headerToken && headerToken.split(' ')[0] !== 'Bearer') {
    return response.status(401).send({ message: 'Invalid token' });
  }

  const token = headerToken.split(' ')[1];
  return firebase
    .auth()
    .verifyIdToken(token)
    .then(() => next())
    .catch((err) =>
      response
        .send({ message: 'Could not authorize, token expired', err })
        .status(403),
    );
}

module.exports = authMiddleware;
