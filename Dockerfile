FROM node:16.4.2-alpine3.12

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
COPY yarn.lock /usr/src/app/
RUN yarn

COPY . /usr/src/app

ENV PORT 4000
EXPOSE $PORT
CMD [ "yarn", "start" ]
