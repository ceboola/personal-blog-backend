require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const multer = require('multer');

const subscribe = require('./routes/subscribe');
const unsubscribe = require('./routes/unsubscribe');
const checkIpUser = require('./routes/checkIpUser');
const checkIp = require('./routes/checkIp');
const checkGeolocation = require('./routes/checkGeolocation');
const healthCheck = require('./routes/healthcheck');
const adminClaimsRouter = require('./routes/adminClaims');
const newsletterRouter = require('./routes/newsletter');
const pageRatingRouter = require('./routes/pageRating');
const authMiddleware = require('./auth-middleware');

const app = express();
const upload = multer();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(upload.array());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  res.header('Access-Control-Max-Age', '3600');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
});

app.use('/healthcheck', healthCheck);
app.use('/subscribe', subscribe);
app.use('/unsubscribe', unsubscribe);
app.use('/checkIpUser', checkIpUser);
app.use('/checkIp', checkIp);
app.use('/checkGeolocation', checkGeolocation);
app.use('/', authMiddleware);
app.use('/adminclaims', adminClaimsRouter);
app.use('/newsletter', newsletterRouter);
app.use('/pageRating', pageRatingRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
