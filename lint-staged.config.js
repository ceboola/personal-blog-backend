module.exports = {
  '*.{js}': [
    'eslint --ignore-path .gitignore .',
    'prettier --write',
    'eslint --fix .',
  ],
  '*.+(json|yml|yaml|css|scss|md|mdx)': ['prettier --write'],
};
