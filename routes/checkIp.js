const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  const ip = req.header('x-forwarded-for') || req.socket.remoteAddress;
  return res.send({ data: req.header });
});

module.exports = router;
