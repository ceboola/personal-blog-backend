const express = require('express');

const router = express.Router();

const firebase = require('../firebase/admin');

router.post('/', async (req, res) => {
  let response = {};
  const db = firebase.firestore().collection('newsletterData');

  await db
    .doc(req.body.email)
    .get()
    .then(async (doc) => {
      const emailExists = doc.exists;
      if (emailExists) {
        response = { newEmail: false };
        return res.status(409);
      }
      await db.doc(req.body.email).set({ email: req.body.email });
      response = { newEmail: true };
      return res.status(200);
    })
    .catch((err) => {
      response = `error: ${err}`;
      return res.status(404);
    });

  return res.send({ message: response });
});

module.exports = router;
