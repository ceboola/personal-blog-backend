const express = require('express');
const fetch = require('node-fetch');
const getIP = require('ipware')().get_ip;

const router = express.Router();

let content;
router.get('/', async (req, res) => {
  const ip = getIP(req);
  const result = await fetch(
    `http://api.ipstack.com/${ip.clientIp}?access_key=be092d2c5904d4c852092529d59c307b`,
  );
  content = await result.json();
  return res.send({ content });
});

module.exports = router;
