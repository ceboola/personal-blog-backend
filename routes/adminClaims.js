const express = require('express');

const router = express.Router();

const checkIfAdmin = require('../checkIfAdmin');
const firebase = require('../firebase/admin');

router.post('/', checkIfAdmin, async (req, res) => {
  const { userId } = req.body;

  await firebase
    .auth()
    .setCustomUserClaims(userId, { admin: true })
    .then(() => res.send({ message: req.body }))
    .catch((error) => res.status(401).send({ message: error }));
  return res.send({ message: req.body });
});

module.exports = router;
