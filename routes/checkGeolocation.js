const express = require('express');
const fetch = require('node-fetch');

const router = express.Router();

let content;
router.post('/', async (req, res) => {
  const result = await fetch(
    `http://api.ipstack.com/${req.body.value}?access_key=be092d2c5904d4c852092529d59c307b`,
  );
  content = await result.json();
  return res.send({ content });
});

module.exports = router;
