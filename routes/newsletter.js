const express = require('express');

const router = express.Router();
const nodemailer = require('nodemailer');
const checkIfAdmin = require('../checkIfAdmin');
const { encrypt } = require('../crypto');

router.post('/', checkIfAdmin, (req, res) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.NEWSLETTER_EMAIL,
      pass: process.env.NEWSLETTER_PASS,
    },
  });

  for (let i = 0; i < req.body.to.length; i += 1) {
    const mailOptions = {
      from: `${req.body.author} <${req.body.authorEmail}>`,
      to: req.body.to[i],
      subject: req.body.subject,
      html: `${req.body.html}
      <br/>
      <div style="background-color:black;color:white">
      want to unsubscribe?
      <a href="https://blog.codesigh.com/unsubscribe/?email=${encrypt(
        req.body.to[i],
      )}">click</a>
      </div>
      `,
    };
    transporter.sendMail(mailOptions, (error) => {
      if (error) {
        res.send(error);
      } else {
        res.send(`Message sent: ${res}`);
      }
    });
  }
  return res.send({ message: 'newsletter sent', body: req.body });
});

module.exports = router;
