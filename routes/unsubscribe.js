const express = require('express');

const router = express.Router();

const { decrypt } = require('../crypto');
const firebase = require('../firebase/admin');

router.post('/', async (req, res) => {
  const hash = decrypt(req.body.email);
  let response = {};

  await firebase
    .firestore()
    .collection('newsletterData')
    .doc(hash)
    .get()
    .then((doc) => {
      const userExists = doc.exists;
      if (userExists) {
        doc.ref.delete();
        response = { text: 'user exists', delete: true };
        return res.status(200);
      }
      response = { text: 'user not exists', delete: false };
      return res.status(404);
    })
    .catch((err) => {
      response = `error: ${err}`;
      return res.status(404);
    });

  return res.send({ message: response });
});

module.exports = router;
