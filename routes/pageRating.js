const express = require('express');

const router = express.Router();

const firebase = require('../firebase/admin');

router.post('/', async (req, res) => {
  let response = {};
  const db = firebase.firestore().collection('postRating');

  await db
    .doc(req.body.url)
    .get()
    .then(async (doc) => {
      const postRatingExists = doc.exists;
      if (postRatingExists) {
        response = { createdPostRating: false };
        return res.status(409);
      }
      await db.doc(req.body.url).set({ url: req.body.url });

      response = { createdPostRating: true };
      return res.status(200);
    })
    .catch((err) => {
      response = `error: ${err}`;
      return res.status(404);
    });

  return res.send({ message: response });
});

module.exports = router;
