module.exports = {
  extends: [
    'airbnb-base',
    'prettier',
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:mocha/recommended',
  ],
  plugins: ['prettier', 'mocha'],
  rules: {
    'prettier/prettier': ['error'],
    'mocha/no-mocha-arrows': 'off',
  },
  parserOptions: {
    ecmaVersion: 2020,
  },

  env: {
    es6: true,
  },
};
